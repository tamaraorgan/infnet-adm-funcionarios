import { ThemeProvider } from 'styled-components'

import theme from './styles/theme'
import Global from './styles/global'

import Routes from './router'

function App() {
  return (
  
      <ThemeProvider theme={theme}>
        <Global />
        <Routes />
      </ThemeProvider>
   
  )
}

export default App
