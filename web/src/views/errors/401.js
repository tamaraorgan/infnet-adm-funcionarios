import { Container, Box } from './style'

import ErrorImg from '../../assets/image/Error.svg'

function Error401() {
  return (
    <Container>
      <img src={ErrorImg} alt="Error"/>
      <Box>
        <h1>404</h1>
        <p>PÁGINA NÃO ENCONTRADA!</p>
      </Box>
    </Container>
  )
}
export default Error401