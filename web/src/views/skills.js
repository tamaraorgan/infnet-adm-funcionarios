import React, { useEffect, useState } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import {  getSkillList, createSkill, updateSkill, deleteSkill } from '../store/skill/skill.action'
import Loading from '../components/Spinner'
import ListSkills from '../components/ListSkills'
import { Row, Col } from 'reactstrap'
import FormSkill from '../components/FormSkills'
import styled from 'styled-components'


const SkillsManagement = () => {
  const dispatch = useDispatch()
  const skills = useSelector(state => state.skill.skills)
  const loading = useSelector(state => state.skill.loading)
  const detail = useSelector(state => state.skill.details)
  const stateModal = useState(false)
  const [modal, setModal] = stateModal
  const stateForm = useState({})
  const [form, setForm] = stateForm
  const stateUpdate = useState(false)
  const [update, setUpdate] = stateUpdate

  useEffect(() => {
    dispatch(getSkillList())
  }, [dispatch])

  const toggle = () => {
    if(modal){
      setUpdate(false)
    }
    setModal(!modal)
  }

  const editSkill = (item) => {
    setForm({ ...item })
    setUpdate(true)
    toggle()

  }
  
  const removeSkill = async skill => {
    await dispatch(deleteSkill(skill))
  }

  const handleSubmit = () => {
    
    const nForm = {
      skill: form.skill,
      image: form.image,
      status: "true"
    }
    dispatch(update ? updateSkill(form.id, nForm) : createSkill(nForm))
    setForm({})
    toggle()
  }

  return (
    <>
      {loading ? (
        <Loading />
      ) : (
        <>
        <SRow>
              <FormSkill
                stateForm={stateForm}
                detail={detail}
                stateModal={stateModal}
                stateUpdate={stateUpdate}
                handleSubmit={handleSubmit}
                toggle={toggle}
              />
            </SRow>
          {!loading && skills.length === 0 ? (
            <div>Não há skills cadastradas.</div>
          ) : (
            <SRow>
            
              <ListSkills
                skills={skills}
                stateUpdate={stateUpdate}
                detail={detail}
                editSkill={editSkill}
                removeSkill={removeSkill}
                toggle={toggle}
              />
          
          </SRow>
          )}
          
          
        </>
      )}
    </>
  )
}

export default SkillsManagement


const SRow = styled(Row)`
display: flex;
flex-direction: column;
width:80%;
`
