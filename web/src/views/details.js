import React, {useEffect, useState} from 'react'
import {useParams} from 'react-router'
import Loading from '../components/Spinner'
import { Row, } from 'reactstrap'
import InformationDetail from '../components/InformationDetail'
import styled from 'styled-components'
import {useDispatch, useSelector} from "react-redux"
import { getSkillDetailsById } from '../store/skill/skill.action'
import { createAllocation, removeAllocation } from '../store/user/user.action'
import ModalSucesso from '../components/ModalSucesso'

const Details = (props) => {
  const { id } = useParams()
  const dispatch = useDispatch()

  const isAdmin = useSelector(state => state.auth.isAdmin)
  const user = useSelector(state => state.auth.user.name)
  const detail = useSelector(state=> state.skill.details)
  const registered = useSelector(state => state.skill.details.registered)
  const loading = useSelector(state => state.skill.loading)
  const allocations = useSelector(state => state.skill.details.allocations)

  const stateModal = useState(false)
  const [modal, setModal] = stateModal
  



  const result = (sucesso) => {
    if('sucesso') {
      setModal(true)
    } else {
      setModal(true)
    }
  }

  const toggle = () => setModal(!modal)
 
  // const toggleModal = () => setModal(false)

  const toggleAllocation = (allocations) => {
    
    if(registered && allocations[0] ) {
      dispatch(removeAllocation(id, allocations[0].id_allocation))
      .then(() => result('sucesso'))
      .catch(() => result('erro'))
    } else {
      dispatch(createAllocation(id))
      .then(() => result('sucesso'))
      .catch(() => result('erro'))
    }
  
  }

  useEffect(() => {
    dispatch(getSkillDetailsById(id))
  }, [dispatch, id])
  
  return (
    
     <>
     {loading ? 
      <Loading/> : 
    
      <NRow>
        <ModalSucesso user={user} modal={modal} toggle={toggle} registered={registered} detail={detail}/>
        <InformationDetail user={user} detail={detail} allocations={allocations} isAdmin={isAdmin} toggleAllocation={toggleAllocation} registered={registered}/>
      </NRow>
      }
    </>
  )
}

export default Details


const NRow = styled(Row)`
display: flex;
flex-direction: column;
width:90%;
flex: 1;
`