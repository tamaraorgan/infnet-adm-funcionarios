import React, { useState, useEffect } from 'react'
import { Button, Container, Row, Col, Label, Input, CardBody, Form } from 'reactstrap';
import { useSelector, useDispatch } from 'react-redux'
import { SCard, SFormGroup, NFormGroup,   SInput, SSpinner, SCardHeader, SAlert  } from './signin_signup/style'
import { updateProfile } from '../store/user/user.action';
import { signOutAction } from '../store/auth/auth.action';


const Profile = () => {

  const profile = useSelector(state => state.auth.user)
  const loading = useSelector(state => state.auth.loading)
  const [form, setForm] = useState({...profile})
  const [success, setSuccess] = useState(false)
  const dispatch = useDispatch()

  const handleChange = (e) => {
    const {name, value} = e.target
    setForm({
      ...form,
      [name]: value
    })
  }

  const updateForm = () => {

    dispatch(updateProfile(form))
    setSuccess(true)  
    setTimeout(() => {
      dispatch(signOutAction())
      setSuccess(false)
    }, 1000);
    
  }

  useEffect(() => {
    
  }, [success])

  const invalidInput = () => {
    const inputs = ['name', 'email']
    const invalid = (label) => form[label] !== ""
    return inputs.some(item => invalid(item))
  }

  return (
    <Container className="d-flex justify-content-center">
    <Row>
      
      <Col sm={12} md={4}>
       <SAlert color="success" isOpen={success} toggle={()=>setSuccess(false)}>
        <div>{form.name} seu perfil foi atualizado.</div>
        <small>Você será redirecionado em 1s</small>
      </SAlert> 
     
       <SCard>
          <SCardHeader tag="h4" className="text-center">Editar Perfil de Usuário</SCardHeader>
          <CardBody>
            <Form>
              <SFormGroup>
                <SInput disabled={loading} type="text" name="name" id="name" onChange={handleChange} value={form.name} placeholder="Digite o primeiro e último nome"/>
              </SFormGroup>
              <SFormGroup>
                <SInput disabled={loading} type="text" name="email" id="email" onChange={handleChange} value={form.email} placeholder="Digite seu e-mail."/>
                </SFormGroup>
                  <NFormGroup>
                  <Label><Input disabled={loading} type="radio" name="type" onChange={handleChange} checked={form.type === '1'} value={"1"}/>Recursos Humanos</Label>
                 
                  <Label><Input disabled={loading} type="radio" name="type" onChange={handleChange} checked={form.type === '2'} value={"2"}/>Colaborador</Label>
                  </NFormGroup>
                  
                  
                  
                  <SFormGroup><Button onClick={updateForm} disabled={!invalidInput()}> {loading ? (<><SSpinner/> Carregando...</>) : "Confirmar alteração"}</Button></SFormGroup>
                
            </Form>
          </CardBody>
        </SCard>
        </Col>
        </Row>
        </Container>
  )
}

export default Profile

