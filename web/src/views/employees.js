import React, { useState, useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import TableUsers from '../components/TableEmployees'
import { getAllEmployees, getAllUsers } from '../store/user/user.action'

const Employees = () => {

   const dispatch = useDispatch()
   const employees = useSelector(state => state.user.employees)
   const stateModal = useState({
    status: false,
    employee: {}
  })
  const [modal, setModal] = stateModal

   useEffect(() => {
     dispatch(getAllEmployees())
   }, [dispatch])

  const toggle = (employee = {}) => setModal({
    status: !modal.status,
    employee: employee
  })
 
  return (
    <div>
     <TableUsers
     modal={modal}
     toggle={toggle}
     employees={employees}/>
    </div>
  )
}

export default Employees
