import { useEffect} from 'react'
import Loading from '../components/Spinner'
import CardSkill from '../components/CardSkill'
import { useDispatch, useSelector } from 'react-redux'
import { getSkillList } from '../store/skill/skill.action'
import { Row, Col, Container} from 'reactstrap'
import styled from 'styled-components'
import { Link } from 'react-router-dom'

function Home() {
 
  const dispatch = useDispatch()
  const skills = useSelector(state => state.skill.skills)
  const loading = useSelector(state=> state.skill.loading)
  const isAdmin = useSelector(state=> state.auth.isAdmin)

  useEffect(() => {
    dispatch(getSkillList()) //funcao da action 
  }, [dispatch])

  const MapSkills = skills =>
    skills.map((skill, i) => <CardSkill key={i} skill={{ ...skill }} />)

    if (loading) {
      return <Loading/>
    }
  return (
       <>
       <SRow>
      {!loading && skills.length === 0 ?
       isAdmin ?
       (
       
         <SCol sm={12} className="d-flex align-items-start mt-4">
           <p>Caro administrador, não há skills disponíveis, cadastre-as<Link className="ml-1" to='/skills'> aqui</Link></p>
         </SCol>
      )
      : ( 
        <SCol>
          <p>Não há skills disponíveis. Aguardar notificação da área de RH.</p>
          </SCol>
        ) 
        
        :
        loading ? <Loading/> : 
       (
         <>
         <Instruction>
         {isAdmin ? <SCol >Caro administrador, selecione uma Skill para visualizar a equipe:</SCol> :
        <SCol>Caro colaborador, faça o seu cadastro nas Skills, as quais possui experiência comprovada: </SCol>
         }
         </Instruction>
        
       <SRow className="flex-grow-1">
       <SCol>{MapSkills(skills)}</SCol>
       </SRow> 
       </>
       )}
        </SRow>
        </>
   
  )}

export default Home



const SRow = styled(Row)`
display: flex;
flex-direction: column;
align-items: center;
flex: 1;
width: 80%;
margin-top: auto;

`
const Instruction = styled(Row)`
display: flex;
flex-direction: column;
flex: 0;
width: 100%;
margin-top: 3vh;
height:20vh;

`

const SCol = styled(Col)`
display: flex;
flex-wrap: wrap;
width:100%;
align-items: flex-start;
justify-content: center;
font-size: 1.5rem;
padding: 1.5rem;

@media (max-width: 768px) {
    height: 100vh;
  }

p{
  text-align: center;
}
`
