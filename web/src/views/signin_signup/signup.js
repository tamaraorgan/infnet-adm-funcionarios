import React, { useEffect, useState } from 'react';
import { signUpAction } from '../../store/auth/auth.action'
import { Button, Container, Row, Col, CardBody, Form, Input, Label } from 'reactstrap';
import { useDispatch, useSelector } from 'react-redux'
import { SCard, SLink, SFormGroup, NFormGroup, SInput, SSpinner, SCardHeader, CardFooter, SAlert  } from './style'

const SignUp = () => {
  const [anyError, setAnyError] = useState(false)
  // const success = useSelector(state => state.auth.success)
  const loading = useSelector(state => state.auth.loading)
  const dispatch = useDispatch();
  const [isOpen, setIsOpen] = useState(false)
  const [form, setForm] = useState({
    name:"",
    email:"",
    password:"",
    type: "2",
  })
  const error = useSelector(state => state.auth.error)
  const registered = useSelector(state => state.auth.registered)

  const handleChange = (e) => {
    const { value, name } = e.target
    setForm({
      ...form,
      [name]:value,
    })

  }

const invalidInput = () => {
  const inputs = ['name', 'email', 'password']
  const invalid = (label) => form[label] !== ""
  return inputs.some(item => invalid(item))
}

const closeAlert = () => {
  setAnyError(false)
  setIsOpen(false)
}


useEffect(() => {
 if(error.length > 1) {
   setAnyError(true)
 }//seta o erro para true ou false, se há um length maior do que zero, retorna true, se nao retorna false.
  setForm({
    name:"",
    email:"",
    password:"",
    type: "2",})
}, [error])

const submitForm = (event) => {
  dispatch(signUpAction(form))
 
}

useEffect(() => {
  if(registered) {
    setIsOpen(true)
    setForm({
      name:"",
      email:"",
      type: "2",
      password:""
    }) 
  }
}, [registered])

  return (
    
    <Container className="d-flex justify-content-center">
    <Row>
      
      <Col sm={12} md={4}>
      <SAlert color="success" isOpen={isOpen} toggle={closeAlert}>
        <div>{form.name} foi cadastrado com sucesso.</div>
        <small>Você será redirecionado em 5s</small>
      </SAlert>
      <SAlert color="danger" isOpen={anyError} toggle={closeAlert}>
        <div>aconteceu um erro</div>
      </SAlert>
       <SCard>
          <SCardHeader tag="h4" className="text-center">Cadastre-se</SCardHeader>
          <CardBody>
            <Form>
              <SFormGroup>
                <SInput disabled={loading} type="text" name="name" id="name" onChange={handleChange} value={form.name || ""} placeholder="Digite o primeiro e último nome"/>
              </SFormGroup>
              <SFormGroup>
                <SInput disabled={loading} type="text" name="email" id="email" onChange={handleChange} value={form.email || ""} placeholder="Digite seu e-mail."/>
                </SFormGroup>
                <SFormGroup>
                  <SInput disabled={loading} type="password" name="password" id="password" onChange={handleChange} value={form.password || ""} placeholder="Defina sua senha"/>
                  </SFormGroup>
                  <NFormGroup>
                  <Label><Input disabled={loading} type="radio" name="type" onChange={handleChange} value={"1"}/>Recursos Humanos</Label>
                 
                  <Label><Input disabled={loading} type="radio" name="type" onChange={handleChange} value={"2"}/>Colaborador</Label>
                  </NFormGroup>
                  
                  
                  
                  <SFormGroup><Button onClick={submitForm} disabled={!invalidInput()}> {loading ? (<><SSpinner/> Carregando...</>) : "Cadastrar"}</Button></SFormGroup>
                
            </Form>
          </CardBody>
          <CardFooter className="text-muted text-center">
            Já tem Cadastro? <SLink to="/signin">Vá para Login</SLink>
          </CardFooter>
        </SCard>
        </Col>
        </Row>
        </Container>
        
  )
}

export default SignUp
