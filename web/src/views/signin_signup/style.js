import styled from 'styled-components'
import { Link } from 'react-router-dom'
import { Card, CardHeader, Input, Spinner, FormGroup, Alert } from 'reactstrap';

export const SCardHeader = styled(CardHeader)`
color: ${props => props.theme.colors.primary};
`
export const SCard = styled(Card)`
min-width: 350px;
border: 1px solid ${props => props.theme.colors.secondary};
-webkit-box-shadow: 1px 7px 10px 0px  rgba(50, 50, 50, 0.75);
-moz-box-shadow:    1px 7px 10px 0px  rgba(50, 50, 50, 0.75);
box-shadow:         1px 7px 10px 0px rgba(50, 50, 50, 0.75);
`
export const SAlert = styled(Alert)`
min-width: 350px;
`

export const CardFooter = styled.div`
padding: 15px 0;
border-top: 0.8px solid ${props => props.theme.colors.tertiary};
`
export const SSpinner = styled(Spinner)`
  margin-right: 0.6rem;
`

export const SLink = styled(Link)`
color: ${props => props.theme.colors.secondary};

:hover {
color: ${props => props.theme.colors.primary};
}
`

export const SFormGroup = styled(FormGroup)`
display: flex;
align-items: center;
justify-content: space-between;
height: 2.5rem;
line-height:2.5rem;
/* padding:0.5rem; */
border-radius: 3px;
border: 0.8px solid ${props => props.theme.colors.tertiary};

svg{
  margin-left:0.5rem;
  color: ${props => props.theme.colors.primary};
}

button{
  width:100%;
  height: 2.5rem;
  line-height:2.5rem;
  margin:0;
  padding:0;
  background-color:${props => props.theme.colors.primary};
}

&:hover{
  background-color:${props => props.theme.colors.background} !important;
}

`
export const NFormGroup = styled(FormGroup)`
display: flex;
flex-direction: row;
align-items: center;
justify-content: space-evenly;
/* height: 2.5rem;
line-height:2.5rem; */
/* padding:0.5rem; */
/* border-radius: 3px;
border: 0.8px solid ${props => props.theme.colors.tertiary}; */

&:hover{
  background-color:${props => props.theme.colors.background} !important;
}

`

export const SInput = styled(Input)`
margin-top:1rem;
margin-bottom: 1rem;
border: none;


&:hover{
  background-color:${props => props.theme.colors.background} !important;
  border: none;
}
&:after{
  background-color:${props => props.theme.colors.white} !important;
}
&:focus{
  box-shadow:none;
}
`
