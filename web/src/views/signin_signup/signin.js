import React, { useState, useEffect } from 'react'
import { FaUser, FaLock } from 'react-icons/fa'
import { Button, Container, Row, Col, CardBody, Form } from 'reactstrap';
import { useDispatch, useSelector } from 'react-redux'
import { signInAction } from '../../store/auth/auth.action'
import { SCard, SFormGroup, SLink, SInput, SSpinner, SCardHeader, CardFooter, SAlert  } from './style'

const SignIn = () => {

  const [anyErr, setAnyErr] = useState(false)

  const dispatch = useDispatch()
  const error = useSelector(state => state.auth.error)
  const loading = useSelector(state => state.auth.loading)

  const [form, setForm] = useState({
    user: "",
    password: ""
  })

  const handleChange = e => {
    const { name, value } = e.target
    setForm({
      ...form,
      [name]: value
    })
  }

  const closeAlert = () => setAnyErr(false)

  const submitForm = () => {
    dispatch(signInAction(form))
  }

  const invalidInput = () =>
    form.user==="" || form.password === ""

  useEffect(() => {
    setAnyErr(error.length > 0)
  }, [error])

  return (
    <Container className="d-flex justify-content-center">
      <Row>
        <Col sm="12" md="4">
          <SAlert color="danger" isOpen={anyErr} toggle={closeAlert}>
            <div>
              <strong>OPS !!! </strong> Aconteceu um erro.
            </div>
            <small>Verifique usuário e senha</small>
          </SAlert>
          <SCard>
            <SCardHeader tag="h4" className="text-center">
              Faça seu Login
            </SCardHeader>
            <CardBody>
              <Form>
                <SFormGroup>
                <FaUser />
                  <SInput
                    disabled={loading}
                    type="email"
                    name="user"
                    onChange={handleChange}
                    value={form.user || ''}
                    placeholder="Digite o seu e-mail"
                  />
                </SFormGroup>
                <SFormGroup>
                <FaLock />
                  <SInput
                    disabled={loading}
                    type="password"
                    name="password"
                    id="password"
                    onChange={handleChange}
                    value={form.password || ''}
                    placeholder="Digite sua senha"
                  />
                </SFormGroup>
                <SFormGroup>
                <Button 
                onClick={submitForm}
                  disabled={invalidInput()}>
                    {loading ? (
                    <>
                      <SSpinner /> Carregando...
                    </>
                  ) : (
                    'Entrar'
                  )}
                </Button>
                </SFormGroup>
              </Form>
            </CardBody>
            <CardFooter className="text-muted text-center">
              Não tem Cadastro? <SLink to="/signup">Cadastre-se</SLink>
            </CardFooter>
          </SCard>
        </Col>
      </Row>
    </Container>
  )
}
export default SignIn