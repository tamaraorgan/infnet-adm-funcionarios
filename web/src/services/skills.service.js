import http from '../config/http'

export const getServiceAllSkills = () => http.get('/skills')

export const getServiceSkillDetailsById = (id) => http.get(`/skills/${id}`)

export const createServiceSkill = (skill) => http.post(`/skills`, skill)

export const updateServiceSkill = (id, data) => http.put(`/skills/${id}`, data)

export const postServiceAllocation = (id, data) => http.post(`/skills/${id}/allocation`, data)

export const deleteServiceSkill = (id) => http.delete(`/skills/${id}`)

export const getServiceAllocation = (id) => http.get(`/skills/${id}/allocation/`)




