import http from '../config/http'

export const getServiceAllUsers = () => http.get('/users')

export const getServiceUser = (id) => http.get(`/users/${id}`)

export const updateServiceUsers = (id, data) => http.put(`/users/${id}`, data)

export const getServiceEmployees = () => http.get('/employees')

export const createServiceAllocation = (id_skill) => http.post(`/skills/${id_skill}/allocation`)

export const removeServiceAllocation = (id_allocation) => http.delete(`/skills/allocation/${id_allocation}`)