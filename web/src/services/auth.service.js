import http from '../config/http.js'

const authService = data => http.post('/sessions', data)

const registerUserService = data => http.post('/users', data)

// const authService = data => ({
//   data: {
//     token:
//       'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6ImV6ZXIubWVsbG9AcHJvZi5pbmZuZXQuZWR1LmJyIiwiaWF0IjoxNjE3NjE3NjQzLCJleHAiOjE2MTc2MTgyNDN9.YRpWemGAIf3ALZI3xzf33OGxmOIDoUCaUys2JY20VwM',
//     user: {
//       name: 'Julia Fremder',
//       email: 'juliafremder@gmail.com',
//       type: '2'
//     }
//   }
// })

export { authService, registerUserService }
