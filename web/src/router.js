import { Router, Switch, Route, Redirect } from 'react-router-dom'
import { useSelector } from 'react-redux'
//config
import { isAuthenticated } from './config/auth'
import history from './config/history'
//Layout
import Layout from './components/Layout'

//views
import Home from './views/home'
import Error404 from './views/errors/404'
import Error401 from './views/errors/401'
import SignIn from './views/signin_signup/signin'
import SignUp from './views/signin_signup/signup'
import Employees from './views/employees'
import Skills from './views/skills'
import Profile from './views/profile'
import Details from './views/details'

//const isAuthenticated = false //mock

const AdminRoute = ({ ...rest }) => {
  if (!isAuthenticated()) {
    return <Redirect to="/signin" />
  }

  const hasAdmin = Object.keys(rest).includes('admin') && !rest.admin //existe a propriedade admin na rota e é false? hasAdmin guarda true or false. Se for true, é porque tem a prop Admin e não é true, ou seja, nao é um administrador.

  if(hasAdmin) {
    return <Redirect to="/error/401"/>

  }

  return <Route {...rest} />
}

const Routes = () => {
const isAdmin = useSelector(state => state.auth.isAdmin)

  return (

  <Router history={history}>
    <Layout>
      <Switch>
        <Route path="/signin" component={SignIn} />
        <Route path="/signup" exact component={SignUp} />
       
        <AdminRoute path="/" exact component={Home} />
        <AdminRoute path="/skill/:id" exact component={Details} />
        <AdminRoute exact path="/profile" component={Profile}/>

        
        <AdminRoute exact path='/employees' admin={isAdmin} component={Employees}/>
        <AdminRoute exact path='/skills' admin={isAdmin} component={Skills}/>
        

        <AdminRoute exact to="/error/401" component={Error401}/>
        <AdminRoute to="/error/404" exact component={Error404} />
        <Redirect from="*" to="/error/404" />
      </Switch>
    </Layout>
  </Router>
)}


export default Routes
