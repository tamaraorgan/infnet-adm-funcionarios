import { removeToken, saveAuth } from '../../config/auth'
import { authService, registerUserService } from '../../services/auth.service'
import history from '../../config/history'
import http from '../../config/http'

export const TYPES = {
  SIGN_IN: 'SIGN_IN',
  SIGN_UP:'SIGN_UP',
  SIGN_OUT: 'SIGN_OUT',
  SIGN_ERROR: 'SIGN_ERROR',
  SIGN_LOADING: 'SIGN_LOADING',
  SIGN_SUCCESS: 'SIGN_SUCCESS'
}
export const signInAction = data => {
  return async dispatch => {
    dispatch({
      type: TYPES.SIGN_LOADING,
      status: true
    })
    try {
      const result = await authService(data)
      if (result.data) {
        saveAuth(result.data)
        http.defaults.headers['token'] = result.data.token
      }
      //enviar informação para o reducer
      dispatch({
        type: TYPES.SIGN_IN,
        data: result.data //o result.data vai entrar na alteracao de estado no switch do reducer (signInAction.SIGN_IN.result.data.token)
      })
      history.push('/')
    } catch (error) {
   
      dispatch({
        type: TYPES.SIGN_ERROR,
        Erro: error //vai para o switch case do reducer, dependendo de qual action.type o reducer altera o state para o que está definido em cada case.
      })
    }
  }
}

export const signUpAction = data => {
  
  return async dispatch => {
    
    dispatch({
      type: TYPES.SIGN_LOADING,
      status: true
    })
    try {
      const result = await registerUserService(data)

      if(result.data) {
        saveAuth(result.data)
        http.defaults.headers['token'] = result.data.token
      }
      //enviar informação para o reducer
      dispatch({
        type: TYPES.SIGN_UP,
        data: result.data //o result.data vai entrar na alteracao de estado no switch do reducer (signUPAction.SIGN_UP.result.data.token)
      })

      

          setTimeout(() => {
            history.push('/')
        }, 3000);
      

      

    } catch (error) {
      console.log('erro enviado', error)
      dispatch({
        type: TYPES.SIGN_ERROR,
        Erro: error //vai para o switch case do reducer, dependendo de qual action.type o reducer altera o state para o que está definido em cada case.
      })
    }
  }
}


export const signOutAction = () => {
  return async dispatch => {
    removeToken()
    dispatch({ type: TYPES.SIGN_OUT })
    history.push('/sessions')
  }
}
