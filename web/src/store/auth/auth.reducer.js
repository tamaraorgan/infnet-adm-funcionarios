import { getToken, getUser } from '../../config/auth'
import { TYPES } from './auth.action'
import {TYPES as TYPES_USERS} from '../user/user.action'

const INITIAL_STATE = {
  isAdmin: getUser().type === '1' || false,
  loading: false,
  token: getToken() || '', //token é uma string
  user: getUser() || {}, //o usuario é um objeto
  error: [], // err is an array of objects errors, to work as this it is necessary to create a new array, spread the old one and add the new data, because it wont see any difference between two arrays with differente length, but will see difference between two different array names.
  registered: false,
}

const reducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case TYPES.SIGN_LOADING:
      state.error = []
      state.loading = action.status
      return state
    case TYPES.SIGN_UP:
      state.loading = false
      state.registered = true
      state.user = action.data.email
      state.isAdmin = action.data.type === '1'
      state.token = action.data.token
      return state
    case TYPES.SIGN_IN:
      state.token = action.data.token
      state.user = action.data.user
      state.isAdmin = action.data.user.type === '1'
      state.loading = false
      return state
    case TYPES_USERS.USERS_PROFILE:
      state.user = {...action.data}
      state.isAdmin = action.data.type === '1'
      return state
    case TYPES.SIGN_ERROR:
      const err = [...state.error, action.Erro]
      state.loading = false
      state.error = err
      return state
    case TYPES.SIGN_OUT:
      state.token = ''
      state.user = {}
      state.isAdmin = false
      state.error = []
      return state
    default:
      return state
  }
}

export default reducer
