import { createServiceSkill, getServiceSkillDetailsById, getServiceAllSkills, deleteServiceSkill, updateServiceSkill } from "../../services/skills.service"

export const TYPES = {
  SKILL_LIST: 'SKILL_LIST',
  SKILL_LOADING: 'SKILL_LOADING',
  SKILL_DETAILS: 'SKILL_DETAILS'
}

export const getSkillList = () => {

  return async (dispatch) => {
    dispatch({ type: TYPES.SKILL_LOADING, status: true})

    try{
      const List = await getServiceAllSkills()
      dispatch({type:TYPES.SKILL_LIST, data: List.data})

    } catch (error) {
      console.log(error)
      dispatch({ type: TYPES.SKILL_LOADING, status:false})
  
    }
  
 }

}

export const getSkillDetailsById = (id) => {

  return async (dispatch, getState) => {
    try {
      const { auth } = getState()
      const result = await getServiceSkillDetailsById(id)

      result.data.registered = result?.data.allocations.some(item => item.email === auth.user.email)
    // console.log('result', result)
    dispatch({
      type: TYPES.SKILL_DETAILS,
      data: result.data
    })
    } catch (error) {
      console.log('getskilldetailsbyid error', error)
      dispatch({ type: TYPES.SKILL_LOADING, status:false})
    }
    

  }

}

export const createSkill = (skill) => {
  return async (dispatch) => {
    dispatch({type: TYPES.SKILL_LOADING, status: true})
    try {
      await createServiceSkill(skill)
      dispatch(getSkillList())
    } catch (error) {
      dispatch({type: TYPES.SKILL_LOADING, status: false})
      console.log('erro no createSkill no skill.action')
      
    }
  }
}

export const updateSkill = (id_skill, skill) => {
  return async (dispatch) => {
    dispatch({type: TYPES.SKILL_LOADING, status: true})
    try {
      await updateServiceSkill(id_skill, skill)
      dispatch(getSkillList())

    } catch (error) {
      dispatch({type: TYPES.SKILL_LOADING, status: false})
      console.log('erro no updateSkill no skill.action')
      
    }
  }
}

export const deleteSkill = (id_skill) => {
  return async (dispatch) => {
    dispatch({type: TYPES.SKILL_LOADING, status: true})
    try{
      await deleteServiceSkill(id_skill)
      dispatch(getSkillList())
    } catch (error) {
      dispatch({type: TYPES.SKILL_LOADING, status: false})
      console.log('erro ao deletar skill da lista de Skills')
    }
  }
}

export const deleteAllocation = (id_employee) => {
  return async (dispatch, getState) => {
    const { skills } = getState()
    dispatch({type: TYPES.SKILL_LOADING, status: true})
    try {
      const result = await deleteServiceSkill(skills.details.allocation, id_employee)

      if(result.status === 200) {
        dispatch(getSkillDetailsById(skills.details.id))
      }

    } catch (error) {
      dispatch({type: TYPES.SKILL_LOADING, status: false})
      console.log('erro ao deletar skill de colaborador')
    }
  }
}