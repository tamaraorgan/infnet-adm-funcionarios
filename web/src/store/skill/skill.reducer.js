import { TYPES } from './skill.action'

const INITIAL_STATE = {
  skills: [],
  loading: false,
  details: {}
}

const reducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case TYPES.SKILL_LOADING:
      state.loading = action.status
      return state;
    case TYPES.SKILL_LIST:
      state.skills = action.data
      state.loading = false
      return state;
    case TYPES.SKILL_DETAILS:
      state.details = action.data
      state.loading = false
      return state
    default:
      return state
  }
}
export default reducer
