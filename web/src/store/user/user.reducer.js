import { TYPES } from './user.action'

const INITIAL_STATE = {
  usersList: [],
  employees: []
}

const reducer = (state = INITIAL_STATE, action) => {
  switch(action.type){
    case TYPES.USERS_ALL:
        state.usersList = action.data
        state.loading = false
        return state
    case TYPES.USERS_EMPLOYEES:
        state.employees = action.data
        state.loading = false
        return state
    default:
      return state
  }
}

export default reducer