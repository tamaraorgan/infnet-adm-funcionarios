import { createServiceAllocation, getServiceAllUsers, getServiceEmployees, getServiceUser, removeServiceAllocation, updateServiceUsers } from "../../services/user.service"
import { getSkillDetailsById } from "../skill/skill.action"

export const TYPES = {
  USERS_ALL: 'USERS_ALL',
  USERS_EMPLOYEES: 'USERS_EMPLOYEES',
  USERS_PROFILE: 'USERS_PROFILE'
}

export const getAllUsers = () => {
  return async (dispatch) => {
    try {
      const usersList = await getServiceAllUsers()
     
      dispatch({
        type: TYPES.USERS_ALL,
        data: usersList.data
      })
    } catch (error) {
      console.log('erro no userAll action', error)
    }
  }
}

export const updateProfile = ({id, ...data}) => {
  return async (dispatch) => {
    try {
      const response = await updateServiceUsers(id, data)
      if(response) {
        const user = await getServiceUser(id)
      

        dispatch({
          type: TYPES.USERS_PROFILE,
          data: user.data
        })
      }
    } catch (error) {
      console.log('erro no updateProfile')
    }
  }
}

export const getAllEmployees = () => {
  return async (dispatch) => {
    try {
      const employeesList = await getServiceEmployees()
      
      dispatch({
        type: TYPES.USERS_EMPLOYEES,
        data: employeesList.data
      })
    } catch (error) {
      console.log('erro no getAllEmployee action', error)
    }
  }
}

export const createAllocation = (id_skill) => {
  return async (dispatch) => {
    try {
      const allocation = await createServiceAllocation(id_skill)
      if(allocation.data) {
        dispatch(getSkillDetailsById(id_skill))
      }
    } catch (error) {
      console.log('erro no createAllocatioon')
    }
  }
}

export const removeAllocation = (id, id_allocation) => {
  return async (dispatch) => {
    try {
      const remove = await removeServiceAllocation(id_allocation)
     
      if (remove && remove.data) {
        dispatch(getSkillDetailsById(id))
      }
    } catch (error) {
      console.log('erro no removeAllocation', error);
    }
  }
}