import { applyMiddleware, combineReducers, createStore } from 'redux'
import { composeWithDevTools } from 'redux-devtools-extension'
import thunk from 'redux-thunk'

//importação dos reducers
import AuthReducer from './auth/auth.reducer'
import SkillReducer from './skill/skill.reducer'
import UserReducer from './user/user.reducer'

const reducers = combineReducers({
  auth: AuthReducer,
  skill: SkillReducer,
  user: UserReducer
})

//middlewares de redux
const middlewares = [thunk]

//compose middlewares with debugging tools
const compose = composeWithDevTools(applyMiddleware(...middlewares))

//create redux store

const store = createStore(reducers, compose)

export default store

//store é o kit do redux
//action => geladeira (primeira etapa)
//reducers => fogão (etapa de transformação)
//alterar estado e disponibilizar => levar para o quarto para comer
