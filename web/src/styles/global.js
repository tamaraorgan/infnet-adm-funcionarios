import { createGlobalStyle } from 'styled-components'

export default createGlobalStyle`
  * {
    margin: 0;
    padding: 0;
    box-sizing: border-box;
  }

  html,
  body {
    font-size: 100%;
    /* overflow: hidden; */
    font-family: 'Segoe UI', Tahoma, Geneva, Verdana, sans-serif;
  }
  .root {
    width: 100vw;
    height: 100vh;
    
  }
  a {
    text-decoration: none;
  }

  li {
    list-style: none;
  }
 
`
