// eslint-disable-next-line import/no-anonymous-default-export
export default {
  // colors: {
  //   white: '#fff',
  //   primary: '#2e4b56',
  //   secondary: '#779d98',
  //   tertiary: '#c0e6e2',
  //   icon: '#39615d',
  //   text: '#2e4b56',
  //   textTitle: '#F7F7FF',
  //   lineGray: '#f1f1f1',
  //   red: '#e3624c',

  //   background: '#ecf6f5',
  //   modal: '#c0e6e299'
  // }
  colors: {
    white: '#fffffa',
    primary: '#65767e',
    secondary: '#7c98b3',
    tertiary: '#637081',
    icon: '#accbe1',
    text: '#536b78',
    textTitle: '#fffffa',
    lineGray: '#f1f1f1',
    red: '#e3624c',

    background: '#e7f2f6',
    modal: '#637081'
  }
  // colors: {
  //   white: '#fffffa',
  //   primary: 'black',
  //   secondary: 'grey',
  //   tertiary: 'light grey',
  //   icon: 'dark grey',
  //   text: 'black',
  //   textTitle: '#fffffa',
  //   lineGray: '#f1f1f1',
  //   red: '#e3624c',

  //   background: '#fafafa',
  //   modal: '#637081'
  // }
}
