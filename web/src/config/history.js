import { createBrowserHistory } from 'history'

const history = createBrowserHistory()

export default history

//cria um contexto no browser, para o sistema ter acesso as rotas.
//o browser router acessava as rotas e disponibilizava para o switch
