const TOKEN_KEY = 'auth_employees_management'

const getToken = () => {
  const data = JSON.parse(localStorage.getItem(TOKEN_KEY)) //transformando o string guardado no historico do browser em json.
  if (data && data.token) {
    return data.token
  }
  return false
}

const getUser = () => {
  const data = JSON.parse(localStorage.getItem(TOKEN_KEY))

  if (data && data.user) {
    return data.user
  }
  return false
}

const isAuthenticated = () => {
  //validar o token de dentro da localStorage
  return getToken() !== false //se o resultado da getToken for um token, é porque está autenticado. Se for falso é porque nao está.
}

const saveAuth = data => localStorage.setItem(TOKEN_KEY, JSON.stringify(data))
//se o data já vem o localStorage, porque é necessário fazer um setItem e salvar lá de novo?

const removeToken = () => localStorage.removeItem(TOKEN_KEY)

export { saveAuth, getToken, getUser, isAuthenticated, removeToken }
