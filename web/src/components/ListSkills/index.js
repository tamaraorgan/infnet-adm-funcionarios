import React, {useState, useEffect} from 'react'
import { ModalTitle } from 'react-bootstrap'
import { IoPencil, IoTrashBin, IoClose, IoCheckmark } from 'react-icons/io5'
import { useDispatch} from 'react-redux'
import {CardImg, Table, Button, Modal, ModalBody, ModalFooter, ModalHeader} from 'reactstrap'
import styled from 'styled-components'
import { getSkillDetailsById } from '../../store/skill/skill.action'

const ListSkills = ({skills, detail, editSkill,  removeSkill}) => {
  const dispatch = useDispatch()
  const [remove, setRemove] = useState(false)
  const [id, setId] = useState(0)

  const toggleModalRemove = (id) => {
    setId(id)
    setRemove(!remove)
  }

  const toggleModal = () => setRemove(false)

  useEffect(() => {
    if(id) {dispatch(getSkillDetailsById(id))}
  }, [id])
 
  console.log(detail)

  return (
    <>
      <STable>
        <Thead>
          <tr>
            <th>Imagem</th>
            <th>Skill</th>
            <th>URL da imagem</th>
            <th>Alterar</th>
          </tr>
        </Thead>

          <Tbody >
          {skills.map((item, i) => (
            <tr key={i}>
              <td>
                <Img src={item.image} alt="imagem desenhada" />
              </td>
              <td>{item.skill}</td>
              <td>{item.image}</td>
              <td className="acoes">
                <SButton onClick={() => editSkill(item)}>
                  <IoPencil />
                </SButton>
                <SButton onClick={() => toggleModalRemove(item.id)}>
                  <IoTrashBin />
                </SButton>
                
              </td>
            </tr>
             ))}
          </Tbody>
       
      </STable>
      <Modal isOpen={remove} toggle={toggleModal}>
              <ModalHeader toggle={toggleModal}>
              
                  <ModalTitle>Deseja excluir {detail.skill}?</ModalTitle>
                  
             
              </ModalHeader>
              <ModalBody>
                Essa skill tem {detail?.allocations?.length || 0} inscrições.
              </ModalBody>
              <ModalFooter>
                {detail?.allocations?.length > 0 ? (
                  'Não poderá excluir. '
                ) : (
                  <Button onClick={() => removeSkill(detail.id, detail)}>
                    Excluir
                    <IoCheckmark />
                  </Button>
                )}
              </ModalFooter>
            </Modal>
    </>
  )
}

export default ListSkills



const SButton = styled(Button)`
    background: ${props => props.theme.colors.primary};
    color: ${props => props.theme.colors.textTitle};
    width: 3rem;
    height: 2rem;
    display: flex;
    align-items: center;
    justify-content: center;
    font-size: 1.2rem;
    cursor: pointer;
    border: none;
`
const STable = styled(Table)`
border: 0.5px solid ${props => props.theme.colors.lineGray};
overflow-x: auto;
-webkit-box-shadow: 1px 7px 10px 0px  rgba(50, 50, 50, 0.75);
-moz-box-shadow:    1px 7px 10px 0px  rgba(50, 50, 50, 0.75);
box-shadow:         1px 7px 10px 0px rgba(50, 50, 50, 0.75);




`
const Img = styled.img`
width: 40px;
`
const Thead = styled.thead`
color: ${props => props.theme.colors.primary};
  background-color: ${props => props.theme.colors.white};
`
const Tbody = styled.tbody`
  border-top: none;
  color: ${props => props.theme.colors.primary};
  background-color: ${props => props.theme.colors.lineGray};

.acoes{
  display: flex;
  button{
    margin-right:0.5rem;
  }
}

  

`