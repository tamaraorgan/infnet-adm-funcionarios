import React from 'react'
import { Link } from 'react-router-dom'
import {Modal, ModalHeader, ModalFooter, ModalBody, Button} from 'reactstrap'

const ModalSucesso = ({modal, toggle, user, registered, detail}) => {
  return (
      <Modal isOpen={modal} toggle={toggle}>
        <ModalHeader toggle={toggle}>{user}</ModalHeader>
        <ModalBody>{registered ? `Parabéns! Você está inscrito na Skill: ${detail.skill}.` : `Atenção! Você cancelou sua inscrição na Skill: ${detail.skill}.`}</ModalBody>
        <ModalFooter><Button color="Link" tag={Link} to='/'>Painel de Skills</Button> </ModalFooter>
      </Modal>
  )
}

export default ModalSucesso
