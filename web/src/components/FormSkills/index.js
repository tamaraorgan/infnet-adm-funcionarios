import { IoAdd, IoCheckmark, IoClose } from 'react-icons/io5'
import styled from 'styled-components'
import {Modal, ModalHeader, CardTitle, ModalFooter, Button, Col} from 'reactstrap'

const FormSkill = props => {
  const [form, setForm] = props.stateForm
  const [modal, setModal] = props.stateModal
  const [update, setUpdate] = props.stateUpdate
  const detail = props.detail

  const handleChangeList = e => {
    setForm({
      ...form,
      [e.target.name]: e.target.value
    })
  }

  const addSkill = () => {
    setForm({})
    props.toggle()
  }

  return (
    <>
      <SCol>
      Clique aqui para adicionar novas Skills <Button onClick={() => addSkill()}>
          <IoAdd />
        </Button>
      </SCol>

      <Modal isOpen={modal} toggle={props.toggle}>
        <SModalHeader>
          {update ? 'Atualizar Skill:' : 'Cadastrar nova Skill:'}
        </SModalHeader>
        <Form onSubmit={props.handleSubmit}>
          <input
            type="text"
            name="skill"
            placeholder="Nome da skill"
            value={form.skill || ''}
            onChange={handleChangeList}
          />
          <input
            type="text"
            name="image"
            placeholder="URL da imagem da skill"
            value={ form.image ||  '' }
            onChange={handleChangeList}
          />
        </Form>
        <ModalFooter>
          <Button color="danger" onClick={props.toggle}>
            <IoClose />
          </Button>
          <Button onClick={props.handleSubmit}>
            <IoCheckmark />
          </Button>
        </ModalFooter>
      </Modal>
    </>
  )
}
export default FormSkill


const Form = styled.form`
  /* width: 100%; */
  display: flex;
  flex-direction: column;

  > input, p {
    flex: 1;
    width: 80%;
    padding: 0.8rem 1rem;
    border: none;
    border-top: 1px solid ${props => props.theme.colors.background};
    border-left: 2px solid ${props => props.theme.colors.background};
    border-bottom: 1px solid ${props => props.theme.colors.background};
  }
  > button {
    background: ${props => props.theme.colors.primary};
    color: ${props => props.theme.colors.textTitle};
    width: 6rem;
    display: flex;
    align-items: center;
    justify-content: center;
    font-size: 1.2rem;
    cursor: pointer;
    border: none;
  }

  @media (max-width: 576px) {
    flex-direction: column;
    justify-content: center;
    align-items: center;

    margin-top: 2rem;

    > input {
      width: 100%;
      margin-bottom: 0.5rem;
    }

    > button {
      width: 100%;
      height: 3rem;
    }
  }
`
const SCol = styled(Col)`
display: flex;
flex-wrap: nowrap;
flex-direction: row;
align-items: center;
justify-content: center;
padding:4rem;
width: 100%;
font-size:2rem;
max-height:20vh;

button {
    background: ${props => props.theme.colors.primary};
    color: ${props => props.theme.colors.textTitle};
    margin:0 2rem;
   
   @media (max-width: 992px) {
     
    font-size: 0.8rem;    
  }
  @media (max-width: 768px) {
    padding: 4px 8px;
    font-size: 0.7rem;
  }
  }

  @media (max-width: 992px) {
    padding: 1rem;
    font-size: 1.5rem;
  }
  @media (max-width: 768px) {
    padding: 0.5rem;
    font-size: 1.2rem;
  }
  @media (max-width: 576px) {
    font-size: 1rem;
  }
`



const SModalHeader = styled(ModalHeader)`
color: ${props => props.theme.colors.primary}
`