import React from 'react'
import { Col } from 'react-bootstrap'
import { IoList } from 'react-icons/io5'
import { Modal, ModalHeader, ModalFooter, ModalBody, Table, Button} from 'reactstrap'
import styled from 'styled-components'

const TableEmployees = ({employees, toggle, modal}) => {

  const MapearSkillsUser = (array) => {

      return (
        <STable >
           
             <Thead>
               <tr >
               <th>Colaborador</th>
               <th>Status</th>
               <th className="text-center">Quantidade de Skills</th>
               <th className="text-center">Ver lista de Skills</th>
               </tr>
             </Thead>
            
               <Tbody >
               {array && array.map((employee, i) => (
                <tr key={i} style={{ cursor: 'pointer' }} >
                  <td>{employee.name}</td>
                  <td>{employee?.type === '1' ? 'ADM' : 'Colaborador'}</td>
                  <td className="text-center">{employee?.allocations?.length === 0 ? '-' : employee?.allocations?.length}</td>
                  <td className="text-center" onClick={() => toggle(employee)}>{employee?.allocations?.length >0 ? <IoList /> : ''}</td>
                  </tr> 
                
               
            ))}
            </Tbody>

        </STable>
      )
  }
  
  
  return (
    <>
      <Col>
      {MapearSkillsUser(employees)}
      </Col>
      <Modal isOpen={modal.status} >
              <ModalHeader toggle={toggle}>{modal.employee.name}</ModalHeader>
              <ModalBody>
                {modal.employee?.allocations?.map((itemSkill, index) => (
                  <div key={index}>{itemSkill.skills.skill}</div>
                ))}
              </ModalBody>
              <ModalFooter>
                
              </ModalFooter>
          </Modal>
    
      </>
       
  )
}

export default TableEmployees

const STable = styled(Table)`
display: flex;
flex-direction: column;
align-items: center;
align-self: center;
justify-self: center;
width: 50vw;
margin: 0;
border: 0.5px solid ${props => props.theme.colors.lineGray};
overflow-x: auto;
-webkit-box-shadow: 1px 7px 10px 0px  rgba(50, 50, 50, 0.75);
-moz-box-shadow:    1px 7px 10px 0px  rgba(50, 50, 50, 0.75);
box-shadow:         1px 7px 10px 0px rgba(50, 50, 50, 0.75);

`

const Thead = styled.thead`
th{
  width: 300px;
  color: ${props => props.theme.colors.primary};
  background-color: ${props => props.theme.colors.white};
}
`
const Tbody = styled.tbody`
border-top: none;
td{
  width: 300px;
  color: ${props => props.theme.colors.primary};
  border-top: none;
  background-color: ${props => props.theme.colors.lineGray};
}
`