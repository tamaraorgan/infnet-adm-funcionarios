import InfoSVG from '../../assets/image/info.svg'

import { BodyModal, ButtonModal, ContainerModal, SectionModal } from './style'

function Modal({ modal, isShow, toggleModal, handleDeleteInput }) {
  return (
    <ContainerModal isShow={isShow}>
      <SectionModal>
        <img src={InfoSVG} alt="" />
        <BodyModal>
          <div className="headerModal">EXCLUIR FUNCIONÁRIO</div>
          <div className="mainModal">
            Deseja excluir o funcionário(a), {modal?.data?.employee_name} ?
          </div>
          <ButtonModal>
            <button >SIM</button>
            <button onClick={toggleModal}>NÃO</button>
          </ButtonModal>
        </BodyModal>
      </SectionModal>
    </ContainerModal>
  )
}

export default Modal
