import {useSelector} from 'react-redux'
import { Link } from 'react-router-dom'
import { Card, CardImg, CardHeader } from 'reactstrap'
import styled from 'styled-components'


function CardSkillRender(props) {
  const { id, image} = props.skill
  const isAdmin = useSelector(state => state.auth.isAdmin)

  
  return (
  
    <SCard >
      {isAdmin ? 
      (
      <SCardHeader>Inscritos: {props.skill.qt_allocations || 0}</SCardHeader> 
        )
       : 
       (
       <SCardHeader>Inscrito:{ props.skill.allocated ? ' Sim' : ' Não'}</SCardHeader> 
       )}
      <SCardImg src={image} alt="imagem desenhada" />
      
      <SLink to={`/skill/${id}`} >ENTRAR</SLink>
    
    </SCard>
   
  )
}

export default CardSkillRender


const SCard = styled(Card)`
  background-color: ${props => props.theme.colors.white};
  margin: 1rem;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  align-items: center;
  border-radius: 1rem;
  padding: 1rem;
  border: 1px solid ${props => props.theme.colors.primary};
  width: 250px;
  height: 250px;
  -webkit-box-shadow: 1px 7px 10px 0px  rgba(50, 50, 50, 0.75);
-moz-box-shadow:    1px 7px 10px 0px  rgba(50, 50, 50, 0.75);
box-shadow:         1px 7px 10px 0px  rgba(50, 50, 50, 0.75);

`
const SCardHeader = styled(CardHeader)`
height: 2rem;
line-height:2rem;
font-size: 1.2rem;
width:100%;
text-align: center;
padding: 0; 
color: ${props => props.theme.colors.primary} !important;
background-color: white;
`

const SCardImg = styled(CardImg)`
max-width:200px;
width:150px;
`

const SLink = styled(Link)`
    color: ${props => props.theme.colors.primary} !important;
    text-decoration: none !important;

    :hover {
      background-color: ${props => props.theme.colors.background};
    border: none;
    border: none;
    border-radius:3px;
    padding: 0 1rem;
    }
 
`

