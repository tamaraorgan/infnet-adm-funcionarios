import { useState } from 'react'
import { FaTrashAlt } from 'react-icons/fa'
import { deleteServiceSkill } from '../../services/skills.service'
import Modal from '../../components/Modal'

import { List, TableTitle, TableBody } from './style'

const ListRegister = ({ register }) => {
  const [modal, setModal] = useState({
    isShow: false,
    data: null
  })

  const handleDeleteInput = () => {
    try {
      if (modal.data.id) {
        deleteServiceSkill(modal.data.id)
        alert('Deletado com sucesso')
    
      }
    } catch (error) {
      alert('Aconteceu um erro, tente novamente!')
    }
  }

  const toggleModal = (data = null) => {
    setModal({
      isShow: !modal.isShow,
      data
    })
  }
  return (
    <List>
      {register && register.length ? (
        <>
          <TableTitle>
            <thead>
              <tr>
                <th>Nome</th>
                <th>E-mail</th>
                <th>Telefone</th>
                <th>Lugar</th>
                <th>Estado</th>
                <th></th>
              </tr>
            </thead>
          </TableTitle>
          <TableBody>
            {register &&
              register.map((item, i) => (
                <tbody key={i}>
                  <tr>
                    <td>{item.employee_name}</td>
                    <td>{item.employee_email}</td>
                    <td>{item.employee_phone}</td>
                    <td>{item.employee_local}</td>
                    <td>{item.employee_state}</td>
                    <td>
                      <button onClick={() => toggleModal(item)}>
                        <FaTrashAlt />
                      </button>
                    </td>
                  </tr>
                </tbody>
              ))}
          </TableBody>
          <Modal
            modal={modal}
            isShow={modal.isShow}
            toggleModal={toggleModal}
            handleDeleteInput={handleDeleteInput}
          />
        </>
      ) : (
        <p>Não existe equipe para essa skill.</p>
      )}
    </List>
  )
}
export default ListRegister

/* <Modal
isShow={modal.isShow}
toggleModal={toggleModal}
deleteEmployee={handleDeleteInput}
/> */
