import React from 'react'
import { useState } from 'react'
import { NavLink } from 'react-router-dom'
import { useSelector, useDispatch } from 'react-redux'
import { FaUsersCog } from 'react-icons/fa'
import { RiCustomerService2Fill } from 'react-icons/ri'
import { AiOutlineMenu, AiOutlineClose } from 'react-icons/ai'
import { IoIosPeople } from 'react-icons/io'
import {
  UncontrolledDropdown,
  DropdownItem,
  DropdownToggle,
  DropdownMenu,
} from 'reactstrap'
import {  IoAppsSharp, IoPersonCircleOutline} from 'react-icons/io5'
import { signOutAction } from '../../store/auth/auth.action'
import LogoSVG from '../../assets/image/adm.svg'
import { isAuthenticated } from '../../config/auth'
// import history from '../../config/history';

// import { MenuContainer, MenuTitle, MenuItem } from './style.js'
import styled from 'styled-components'

function Menu({list}) {
  const [isActive, setIsActive] = useState(false)
  const isAdmin = useSelector(state => state.auth.isAdmin)
  const dispatch = useDispatch()
  const user = useSelector(state => state.auth.user)
  
  

  const logout = () => {
    dispatch(signOutAction())
  }

  function handleOpenMenu() {
    setIsActive(!isActive)
  }

  return (
    <>
      <MenuContainer>

        <MenuTitle>
          {isActive ? (
            <>
              <div className="image">
                <img src={LogoSVG} alt="ADM" />
              </div>
              <button onClick={handleOpenMenu}>
                <AiOutlineClose />
              </button>
            </>
          ) : (
            <button onClick={handleOpenMenu}>
              <AiOutlineMenu />
            </button>
          )}
        </MenuTitle>
        
        
        <MenuItem show={isActive}>
          
              
        {isAuthenticated() ? (
          //********************************* */
          <>
          <NavLink exact
          activeClassName="no-active"
          className="navbar_link"
          to="/">
            <div className="button alink">
              <IoPersonCircleOutline /> 
            </div>
            <div className="description">
            <UncontrolledDropdown >
              <SDropdownToggle nav>
                {`${user.name} `}
              </SDropdownToggle>
              <DropdownMenu className="border-0 theme-primary">
                
                <DropdownItem>
                  <SNavLink exact
           activeClassName="no-active"
            className="navbar_link"
            to="/profile">Perfil</SNavLink>
            </DropdownItem>
            <DropdownItem divider />
            <DropdownItem><SNavLink exact
            to="/signin" onClick={logout}>Sair</SNavLink></DropdownItem>
              </DropdownMenu>
            </UncontrolledDropdown>
            </div>
          
            </NavLink>
             
             <NavLink
               exact
               activeClassName="active"
               className="navbar_link"
               to="/"
             >
               <div className="button alink">
                 <IoAppsSharp/>
                 
               </div>
               <div className="description">Painel </div>
             </NavLink>
              </>
        ) 
        
        : 
        
        (
          <NavLink exact
          activeClassName="no-active"
          className="navbar_link"
          to="/signin">
            <div className="button alink">
              <IoPersonCircleOutline /> 
            </div>
          <div className="description">Login </div>
          </NavLink>
        )} 



        
   
        
         
          {isAdmin ? (
            <>
          <NavLink
            exact
            activeClassName="active"
            className="navbar_link"
            to="/skills"
          >
            <div className="button alink" >
            <FaUsersCog />
            </div>
            <div className="description">Skills </div>
          </NavLink> 
          <NavLink
          exact
          activeClassName="active"
          className="navbar_link"
          to="/employees">
            <div className="button alink" >
              <IoIosPeople />
            </div>
            <div className="description">{`Colaboradores `}</div>
            </NavLink>
            </>
            ) : ""}
         
        </MenuItem>
      </MenuContainer>
    </>
  )
}
export default Menu

const MenuContainer = styled.header`
  height: 100vh;
  width: auto;
  min-width: 8rem;

  @media (max-width: 576px) {
    width: 100%;
    height: 4rem;
  }
`
const MenuTitle = styled.div`
  width: 100%;
  height: 4rem;
  display: flex;
  justify-content: center;
  align-items: center;
  padding: 0 0.5rem;
  border: 1px solid ${props => props.theme.colors.lineGray};
  .image {
    width: 13rem;
    flex: 1;
    padding-left: 0.5rem;
  }
  button {
    width: 3rem;
    height: 3rem;
    border: none;
    background: none;
    :focus {
      outline: none !important;
      border: 2px solid ${props => props.theme.colors.primary};
    }
  }
  svg {
    font-size: 1.3rem;
    color: ${props => props.theme.colors.primary};
    :hover {
        color: ${props => props.theme.colors.secondary};
        }
  }
  @media (max-width: 576px) {
    display: none;
  }
`
const MenuItem = styled.nav`
  
  a {
    color: ${props => props.theme.colors.text};
    display: flex;
    cursor: pointer;
    
    .button {
      background: none;
      border: none;
      width: 8rem;
      height: 4rem;
      display: flex;
      align-items: center;
      justify-content: center;
      
      svg {
        font-size: 1.3rem;

        :hover {
        color: ${props => props.theme.colors.secondary};
        }
      }
     
    }
    :hover {
        color: ${props => props.theme.colors.secondary};
        
        }
    
  }
  
  .theme-primary{
    background-color: ${props => props.theme.colors.primary}

  }

  .active {
    background-color: ${props => props.theme.colors.background};
  
  }
  .no-active{
    color:${props => props.theme.colors.primary};
  }
  .description {
      display: ${props => (props.show ? 'flex' : 'none')};
      width: 100%;
      align-items: center;
      justify-content: flex-start;
      font-size: 1.25rem;
      
    }
  

  @media (max-width: 576px) {
    display: flex;
    a {
      flex: 1;
    }
    .description {
      display: none;
    }
  }
`
const SDropdownToggle = styled(DropdownToggle)`
  color: ${props => props.theme.colors.textTitle};
  text-decoration: none;
  padding: 0;

  :hover {
    color: ${props => props.theme.colors.secondary};
  }
  
`
const SNavLink = styled(NavLink)`
  color: ${props => props.theme.colors.textTitle} !important;

  :hover{
    color: ${props => props.theme.colors.primary} !important;
  }
`
