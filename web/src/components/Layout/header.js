import React , {useState, useEffect, useCallback} from 'react'
import { useLocation } from 'react-router';
import styled from 'styled-components'

const Header = () => {
  const [title, setTitle] = useState("");
  const result = useLocation().pathname
  document.title = `ADM | ${title}`
  
  const getTitle = useCallback(async (result) => {
    try {      
      
      let _title = result.replace('/', '')
        _title = _title.indexOf('/') > 0 ?  _title.substring(0, _title.indexOf('/')) : _title
        
        console.log('_title', _title)
        
        switch (_title) {
          case '':
            _title = 'Painel'
            break
          case 'signin':
            _title = 'Login'
            break
          case 'signup':
            _title = 'Cadastro'
            break
            case 'profile':
            _title = 'Perfil'
            break
          case 'skill':
            _title = 'Skills'
            break
          case 'skills':
            _title = 'Skills'
            break
          case 'employees':
            _title = 'Colaboradores'
            break
          default:
            _title = 'Erro'
            break
        }
       setTitle(_title)
        
    } catch (error) {
      setTitle('Erro')
    }
  }, [window.location.pathname])

  useEffect(() => {
    getTitle(result)
  }, [getTitle])
  
 return (
    <Title >
      <div className="container text-center">Intranet <span className="d-sm-inline d-none">- uso restrito aos colaboradores.</span></div>
         
    </Title>
  )
}

export default Header

const Title = styled.div`
  width: 100%;
  height: 4rem;
  background-color: ${props => props.theme.colors.primary};
  color: ${props => props.theme.colors.textTitle};
  display: flex;
  align-items: center;
  justify-content: center;
  padding: 0 1rem;
  font-size: 1.5rem;

  @media (max-width: 576px) {
    text-align: center;
    justify-content: center;
    height:2rem;
    font-size:1.2rem;
  }
`
