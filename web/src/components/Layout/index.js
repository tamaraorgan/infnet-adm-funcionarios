import React, {useState, useEffect} from 'react'
import Menu from '../Menu'
import Header from './header'
import styled from 'styled-components'

function Layout(props) {
  
 
    
  return (
    <SContainer>
      <Menu/>
      <SMain>
        <Title />
        <Box>
        {props.children}
        </Box>
      </SMain>
    </SContainer>
  )
}
export default Layout


const SContainer = styled.div`
display: flex;
@media (max-width: 576px) {
    flex-direction: column;   
  }
`
const SMain = styled.main`
width:100%;
display: flex;
flex-direction: column;

`
const Title = styled(Header)`
display: flex;
flex:0;
flex-direction: column;
justify-self: flex-start;
`
const Box = styled.div`
display: flex;
flex-direction: column;
align-items: center;
justify-content: center;
flex: 1;
background-color: ${props => props.theme.colors.background};
@media (max-width: 576px) {
    overflow-y:scroll;   
  }
`
