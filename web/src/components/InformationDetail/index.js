import React from 'react'
import {Link} from 'react-router-dom'
import styled from 'styled-components'
import {CardImg, CardFooter,CardBody,CardTitle, Card, Button,Col, Table} from 'reactstrap'
import { IoAppsSharp } from 'react-icons/io5'
import { FaUsersCog } from 'react-icons/fa'

const InformationDetail = ({detail, isAdmin, registered, user, allocations, removeSkill, toggleAllocation}) => {
  // console.log(detail, 'informationDetail')
  return (
    <>
    <SCol>
    <SCard>
      <SCardBody>
      <SCardImg src={detail.image} alt={detail.skill} />
      <SCardTitle>{detail.skill}</SCardTitle>
        </SCardBody>
        
          {isAdmin ? 
          <SCardFooter >
          <Link  to='/'> <IoAppsSharp/> Painel</Link> <Link  to='/employees'> <FaUsersCog /> Colaboradores</Link>
          </SCardFooter>
          :
          <SCardFooter >
          <p > {registered ? `Olá, ${user}, você está inscrito, deseja cancelar inscrição nessa Skill? ` : `Olá, ${user}, você ainda não se inscreveu nessa Skill, deseja inscrever-se? `} {registered ? <Button className="ml-2" style={{'backgroundColor': '#e3624c'}} onClick={() => toggleAllocation(allocations)}>Cancelar</Button> : <Button className="ml-2"  style={{'backgroundColor': '#7c98b3'}} onClick={() => toggleAllocation(allocations)}>Inscrever-se</Button>}</p> 
          </SCardFooter>
          }
          
    </SCard>
    </SCol>
    
 
  <SCol>
  {isAdmin ? 
  (
    <>
      {detail.allocations && detail.allocations.length ? (
        <STable>
          
            <Thead>
              <tr>
                <th>Matrícula</th>
                <th>Colaborador</th>
                <th className="text-center">Total</th>
              </tr>
            </Thead>
      
          
            {detail.allocations.map((item, i) => (
                <Tbody key={i}>
                  <tr>
                    <td>{item.id}</td>
                    <td tag={Link} to="/employees">{item.name}</td>
                    <td className="text-center">-</td>
                  </tr>
                </Tbody>
              )
              )}
              <Tbody>
                  <tr>
                    <td>Total:</td>
                    <td></td>
                    <td className="text-center">{detail.allocations?.length || 0}</td>
                  </tr>
              </Tbody>
              
          </STable>
        )  : (
          <div>
            <p>Não há colaborador inscritos nessa Skill.</p>
            </div>
          )}
          </>
        ) : (
          
         ''
        )}
  </SCol>
 
  </>
  )
}

export default InformationDetail

const SCardBody = styled(CardBody)`
display: flex;
align-items: center;
justify-content: center;
flex-wrap: wrap;

`
const SCardTitle = styled(CardTitle)`
font-size: 4rem;
display: flex;
flex:1;
flex-direction: column;
align-items: center;
color: ${props => props.theme.colors.primary};
`
const SCard = styled(Card)`
padding: 0;
display: flex;
flex-wrap: wrap;
width: 50vw;
align-items: center;
margin-top: auto;
margin-bottom: auto;
-webkit-box-shadow: 1px 7px 10px 0px  rgba(50, 50, 50, 0.75);
-moz-box-shadow:    1px 7px 10px 0px  rgba(50, 50, 50, 0.75);
box-shadow:         1px 7px 10px 0px  rgba(50, 50, 50, 0.75);
@media(max-width: 992px){
  width: auto;
}
`
const SCardImg = styled(CardImg)`
width: 250px;
flex: 0;
`
const SCardFooter = styled(CardFooter)`
width: 100%;
display: flex;
justify-content: space-around;
align-items: center;

a{
  color: ${props => props.theme.colors.primary};
  font-weight: bold;
  display: flex;
  justify-content: center;
  align-items: center;
  
  :hover{
    color: ${props => props.theme.colors.secondary};
  }
  svg{
    margin-right: 0.5rem;
  }
}

`

const STable = styled(Table)`
display: flex;
flex-direction: column;
align-items: center;
align-self: center;
justify-self: center;
max-width:50vw;
margin: 0;
border: 0.5px solid ${props => props.theme.colors.lineGray};
overflow-x: auto;
-webkit-box-shadow: 1px 7px 10px 0px  rgba(50, 50, 50, 0.75);
-moz-box-shadow:    1px 7px 10px 0px  rgba(50, 50, 50, 0.75);
box-shadow:         1px 7px 10px 0px rgba(50, 50, 50, 0.75);

@media(max-width: 992px){
  max-width: 100vw;
}

`

const Thead = styled.thead`

th{
  width: 300px;
  color: ${props => props.theme.colors.primary};
  background-color: ${props => props.theme.colors.white};
}
`
const Tbody = styled.tbody`
border-top: none;
td{
  width: 300px;
  color: ${props => props.theme.colors.primary};
  border-top: none;
  background-color: ${props => props.theme.colors.lineGray};
}
`
const SCol=styled(Col)`
display: flex;
flex-direction: column;
align-self: center;
align-items: center;
`