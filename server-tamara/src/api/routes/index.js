const { Router } = require('express')
const { name, version } = require('../../../package.json')
const skillsRouterV1 = require('./v1/skills.router')
const allocationRouterV1 = require('./v1/allocation.router')
const usersRouterV2 = require('./v2/users.router')
const skillsRouterV2 = require('./v2/skills.router')
const allocationsRouterV2 = require('./v2/allocation.router')

module.exports = app => {
  const routerV1 = Router()
  const routerV2 = Router()

  app.get('/', (req, res) => {
    res.send({ name, version })
  })

  skillsRouterV1(routerV1)
  allocationRouterV1(routerV1)

  usersRouterV2(routerV2)
  skillsRouterV2(routerV2)
  allocationsRouterV2(routerV2)

  app.use('/v1', routerV1)
  app.use('/v2', routerV2)
}
