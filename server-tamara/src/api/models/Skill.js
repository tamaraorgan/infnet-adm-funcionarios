module.exports = (sequelize, DataTypes) => {
  const Skill = sequelize.define(
    'Skill',
    {
      id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
        unique: true,
        allowNull: false
      },
      skill: {
        type: DataTypes.TEXT,
        allowNull: false
      },
      image: {
        type: DataTypes.TEXT,
        allowNull: false
      },
      status: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: true
      }
    },
    {
      undescored: true,
      paranoid: true,
      timestamps: false,
      tableName: 'tskill'
    }
  )

  Skill.associate = function (models) {
    Skill.hasMany(models.Allocation, {
      foreignKey: 'id_skill',
      as: 'allocations'
    })
  }

  return Skill
}
