module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define(
    'User',
    {
      id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
        unique: true,
        allowNull: false
      },
      name: DataTypes.TEXT,
      email: DataTypes.TEXT,
      // phone: DataTypes.TEXT,
      // local: DataTypes.TEXT,
      // state: DataTypes.TEXT,
      type: DataTypes.TEXT,
      password: DataTypes.TEXT
    },
    {
      undescored: true,
      paranoid: true,
      timestamps: false,
      tableName: 'tuser'
    }
  )
  User.associate = function (models) {
    User.hasMany(models.Allocation, {
      foreignKey: 'id_user',
      as: 'allocations'
    })
  }

  return User
}
