const db = require('../models/index.js')

/*********************** Filto para listar por Perfil  *************************/
const listByProfileUser = async (user_type, user_id) => {
  const resultDB = await db.Skill.findAll({
    include: {
      model: db.Allocation,
      as: 'allocations'
    }
  })
  let result = []
  if (Number(user_type) === 2) {
    result = resultDB.map(item => {
      const { id, skill, image, status, allocations } = item
      const resultFilter = allocations.filter(allocation => {
        return Number(allocation.id_user) === Number(user_id)
      })

      return {
        id,
        skill,
        image,
        status,
        allocated: resultFilter.length > 0 ? true : false
      }
    })
  } else {
    result = resultDB.map(item => {
      const { id, skill, image, status, allocations } = item

      return {
        id,
        skill,
        image,
        status,
        qt_allocations: allocations.length
      }
    })
  }

  return result
}
/*********************** Localiza Skill por Perfil  *************************/
const searchSkillByProfileUser = async (id, user_type, user_id) => {
  const resultDB = await db.Skill.findOne({
    where: {
      id
    },
    include: [
      {
        model: db.Allocation,
        as: 'allocations',
        include: [
          {
            model: db.User,
            as: 'users'
          }
        ]
      }
    ]
  })
  let result = resultDB?.allocations

  if (Number(user_type) === 2) {
    result = result.filter(item => {
      return Number(item.id_user) === Number(user_id)
    })
  }

  const allocations = result.map(item => {
    return {
      id_allocation: item.id,
      // employee: {
      id: item.users.id,
      name: item.users.name,
      email: item.users.email
      // }
    }
  })

  return {
    id: resultDB?.id,
    skill: resultDB?.skill,
    image: resultDB?.image,
    allocations
  }
}

// const validateIfTheNameAlreadyExists = async skill => {
//   var result = await db.Skill.findOne({
//     where: {
//       name: Sequelize.where(
//         Sequelize.fn('LOWER', Sequelize.col('name')),
//         'LIKE',
//         skill
//       )
//     }
//   })
//   return result ? true : false
// }

/*********************** Verifica se o skill ja existe  *************************/
const validateIfTheNameAlreadyExists = async skill => {
  const result = await db.Skill.findOne({
    where: {
      skill
    }
  })

  return result ? true : false
}
/*********************** Verifica se o id ja existe  *************************/
const validateIfTheIdAlreadyExists = async id => {
  const result = await db.Skill.findOne({
    where: {
      id
    }
  })

  return result ? true : false
}

/****************************** Cria uma skill  *******************************/
const createSkill = async (skill, image) => {
  const skills = {
    skill,
    image
  }
  return await db.Skill.create(skills)
}

/****************************** Altera uma skill  *****************************/
const UpdateSkill = async (id, { skill, image, status }) => {
  const skills = {
    skill,
    image,
    status
  }
  return db.Skill.update({ ...skills }, { where: { id } })
}

/***************************** Deleta uma skill  ******************************/
const deleteSkill = id => {
  return db.Skill.destroy({
    where: {
      id
    }
  })
}

module.exports = {
  UpdateSkill,
  createSkill,
  validateIfTheNameAlreadyExists,
  listByProfileUser,
  searchSkillByProfileUser,
  validateIfTheIdAlreadyExists,
  deleteSkill
}
