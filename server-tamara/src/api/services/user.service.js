const md5 = require('md5')
const jwt = require('jsonwebtoken')
const db = require('../models/index.js')

const hashSecret = process.env.CRYPTO_KEY

/******************* Procura por email e devolve o usuário ********************/
const searchUsersToEmailService = async email => {
  const users = await db.User.findOne({
    where: {
      email
    }
  })
  return users
}

/************************* Cria a senha criptografada *************************/
const createHash = password => {
  return md5(password + hashSecret)
}

/********* Procura o usuário por email e senha e returna um booleano **********/
const usersExistsService = async (user, password) => {
  const users = await db.User.findOne({
    where: {
      email: user,
      password: createHash(password)
    }
  })

  return users ? true : false
}

/************************** Cria um Usuário com token *************************/
const createCredentialService = async userEmail => {
  try {
    const users = await searchUsersToEmailService(userEmail)
    const { id, name, email, type } = users

    const credential = {
      token: jwt.sign({ email: users.email }, process.env.JWT_KEY, {
        expiresIn: `${process.env.JWT_VALID_TIME}d`
      }),
      user: {
        id,
        name,
        email,
        type
      }
    }
    return credential
  } catch (error) {
    return response.status(500).send({ error: 'Internal server error!!' })
  }
}

/********************* Procura o email devolve um booleano ********************/
const validateEmailAlreadyExistsService = async (email, id = 0) => {
  const validateEmail = await searchUsersToEmailService(email)

  if (id === 0) {
    return validateEmail ? true : false
  }
  if (validateEmail) {
    if (validateEmail.id === id) {
      return false
    }
    return true
  } else {
    return false
  }
}

/***************************** Cria um usuário  *******************************/
const createUsersService = async ({ name, email, type, password }) => {
  const users = {
    name,
    email,
    type,
    password: createHash(password)
  }
  return db.User.create(users)
}
/***************************** Cria um usuário  *******************************/
const updateUsersService = async (id, { name, email, type }) => {
  return db.User.update(
    {
      name,
      email, 
      type
    },
    { where: { id } }
  )
}
/*********************** Listar Usuário por Skill  ****************************/
const listUserService = async () => {
  const resultBD = await db.User.findAll({
    where: {
      type: '2'
    },
    include: {
      model: db.Allocation,
      as: 'allocations',
      include: {
        model: db.Skill,
        as: 'skills'
      }
    }
  })
  const result = resultBD.map(item => {
    const { id, name, email, type, allocations } = item

    const resultAllocations = allocations.map(itemAllocation => {
      const { id, skills } = itemAllocation

      return {
        id,
        skills: {
          id: skills.id,
          skill: skills.skill
        }
      }
    })
    return {
      id,
      name,
      email,
      type,
      qtd_allocations: resultAllocations.length,
      allocations: resultAllocations
    }
  })
  return result
}

const listUserById = async (idUser) => {
  const result = await db.User.findOne({
    where: { id: idUser },
    include: {
      model: db.Allocation,
      as: 'allocations',
      include: {
        model: db.Skill,
        as: 'skills'
      }
    }
  })
  const { id, name, email, type, allocations } = result

  const resultAllocations = allocations.map(item => {
    const { id, skills } = item
    return {
      id,
      skill: {
        id: skills.id,
        name: skills.skill
      }
    }
  })
  return {
    id,
    name,
    email,
    type,
    qtd_allocations: resultAllocations.length,
    allocations: resultAllocations
  }
}

module.exports = {
  searchUsersToEmailService,
  usersExistsService,
  createCredentialService,
  validateEmailAlreadyExistsService,
  createUsersService,
  updateUsersService,
  listUserService,
  listUserById
}
