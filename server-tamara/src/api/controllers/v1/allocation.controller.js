const db = require("../../models/index.js")


const getAllocation = async (request, response) => {
  const result = await db.Allocation.findAll({})

  response.status(200).send(
    result.map(item => {
      const {
        id,
        employee_name,
        employee_email,
        employee_phone,
        employee_local,
        employee_state
      } = item

      return {
        id,
        employee_name,
        employee_email,
        employee_phone,
        employee_local,
        employee_state
      }
    }) || []
  )
}

const postAllocationSkill = async (request, response) => {
  try {
    const { id } = request.params
    const body = request.body

    const allocations = {
      id_skill: id,
      employee_name: body.employee_name,
      employee_email: body.employee_email,
      employee_phone: body.employee_phone,
      employee_local: body.employee_local,
      employee_state: body.employee_state
    }

    await db.Allocation.create(allocations)

    response.status(200).send({ message: "Allocations successfully created."})
  } catch (error) {
    response.status(500).send({ message: 'Something went wrong...' })
  }
}

const deleteAllocation = async (request, response) => {
  try {
    const { id_allocation } = request.params

    await db.Allocation.destroy({
      where: {
        id: id_allocation
      }
    })

    response.status(200).send({ message: 'Deleted allocation: ' + id_allocation })
  } catch (error) {
    response.status(500).send({ message: 'Allocation was not deleted.' })
  }
}

module.exports = {
  getAllocation,
  postAllocationSkill,
  deleteAllocation
}
