const db = require('../../models/index')

const {
  usersExistsService,
  createCredentialService,
  validateEmailAlreadyExistsService,
  createUsersService,
  updateUsersService,
  listUserService,
  listUserById
} = require('../../services/user.service')

const authUser = async (request, response) => {
  try {
    const { user, password } = request.body

    const result = await usersExistsService(user, password)

    if (!result) {
      response.status(401).send({ error: 'Incorrect username or password' })
    }

    var credential = await createCredentialService(user)

    return response.status(200).send(credential)
  } catch (error) {
    return response.status(500).send({ error: 'Internal server error!' })
  }
}
const createUser = async (request, response) => {
  try {
    const { body } = request

    const emailAlreadyExists = await validateEmailAlreadyExistsService(
      body.email
    )

    if (emailAlreadyExists) {
      return response.status(400).send({ error: 'E-mail already registered' })
    }

    await createUsersService(body)

    const { id, name, email, type } = body

    return response.status(200).json({
      id,
      name,
      email,
      type
    })
  } catch (error) {
    return response.status(500).send({ error: 'Internal server error!!' })
  }
}
const getAllUsers = async (request, response) => {
  try {
    const result = await db.User.findAll({})

    response.status(200).send(
      result.map(item => {
        const { id, name, email, type } = item

        return {
          id,
          name,
          email,
          type
        }
      }) || []
    )
  } catch (error) {
    return response.status(500).send({ message: 'Internal server error!!' })
  }
}

const updateUsers = async (request, response) => {
  try {
    const { id } = request.params
    const { body } = request

    if (Number(id) !== Number(request.user.id)) {
      return response.status(400).send({ message: 'Operation not allowed' })
    }

    const validateEmail = await validateEmailAlreadyExistsService(
      body.email,
      id
    )
    if (validateEmail) {
      return response.status(400).send({ message: 'E-mail already registered' })
    }

    await updateUsersService(id, body)

    return response.status(200).send({ message: 'Successful change' })
  } catch {}
}

const listUsersAndSkills = async (request, response) => {
  try {
    const users = await listUserService()
    return response.status(200).json(users)
  } catch (error) {
    return response.status(500).send({ message: 'Internal server error!!' })
  }
}

const listUsersById = async (request, response) => {
  try {
    const { id } = request.params
    const user = await listUserById(id)

    return response.status(200).json(user)
  } catch (error) {
    return response.status(500).send({ message: 'Internal server error!!' })
  }
}

module.exports = {
  updateUsers,
  getAllUsers,
  createUser,
  authUser,
  listUsersAndSkills,
  listUsersById
}
