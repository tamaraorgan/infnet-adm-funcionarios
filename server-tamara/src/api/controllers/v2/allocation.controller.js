const db = require('../../models/index.js')
const { validateIfTheIdAlreadyExists } = require('../../services/skill.service')
const {
  createAllocation,
  allocationIfTheIdAlreadyExists,
  validateAllocationByUser,
  deleteAllocation,
  validateUserAlreadyExists
} = require('../../services/allocation.service')

const getAllocation = async (request, response) => {
  const result = await db.Allocation.findAll({})

  response.status(200).send(result)
}

const postAllocationSkill = async (request, response) => {
  try {
  const { id_skill } = request.params
  const { user } = request

  const skillAlreadyExists = validateIfTheIdAlreadyExists(id_skill)

  if (!skillAlreadyExists) {
    return response.status(422).send({ message: 'Skill not found.' })
  }

  const validateSubscription = await validateUserAlreadyExists(
    id_skill,
    user.id
  )

  if (validateSubscription) {
    return response.status(422).send({ error: 'User already exists' })
  }

  await createAllocation(id_skill, user.id)

  return response
    .status(200)
    .send({ message: 'Allocations successfully created.' })
  } catch (error) {
    return response.status(500).send({ message: 'Something went wrong...' })
  }
}

const deleteAllocations = async (request, response) => {
  try {
    const { id_allocation } = request.params
    const { user } = request

    const AllocationAlreadyExists = await allocationIfTheIdAlreadyExists(
      id_allocation
    )

    if (!AllocationAlreadyExists) {
      return response.status(422).send({ message: 'Allocation not found.' })
    }

    const UserValidation = await validateAllocationByUser(
      id_allocation,
      user.id
    )
    if (!UserValidation) {
      return response
        .status(422)
        .send({ message: 'Operation cannot be performed.' })
    }

    await deleteAllocation(id_allocation)

    response
      .status(200)
      .send({ message: 'Deleted allocation: ' + id_allocation })
  } catch (error) {
    response.status(500).send({ error: 'Allocation was not deleted.' })
  }
}

module.exports = {
  getAllocation,
  postAllocationSkill,
  deleteAllocations
}
