'use strict'

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('tallocation', {
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
        unique: true,
        allowNull: false
      },
      id_skill: {
        type: Sequelize.INTEGER,
        references: { model: { tableName: 'tskill' }, key: 'id' },
        onDelete: 'CASCADE'
      },
      employee_name: {
        type: Sequelize.TEXT,
        allowNull: false
      },
      employee_email: {
        type: Sequelize.TEXT,
        allowNull: false
      },
      employee_phone: {
        type: Sequelize.TEXT,
        allowNull: false
      },
      employee_local: {
        type: Sequelize.TEXT,
        allowNull: false
      },
      employee_state: {
        type: Sequelize.TEXT,
        allowNull: false
      },
      status: {
        allowNull: false,
        type: Sequelize.BOOLEAN,
        defaultValue: true
      },
    })
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.dropTable('tallocation')
  }
}
