'use strict'

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('tuser', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      name: {
        allowNull: false,
        type: Sequelize.TEXT
      },
      email: {
        type: Sequelize.TEXT,
        allowNull: false
      },
      // phone: {
      //   type: Sequelize.TEXT,
      //   allowNull: false
      // },
      // local: {
      //   type: Sequelize.TEXT,
      //   allowNull: false
      // },
      // state: {
      //   type: Sequelize.TEXT,
      //   allowNull: false
      // },
      type: {
        type: Sequelize.TEXT,
        allowNull: false
      },
      password: {
        type: Sequelize.TEXT,
        allowNull: false,
      },
      created_at: {
        type: Sequelize.DATE,
        defaultValue: Sequelize.literal('NOW()')
      },
      updated_at: {
        type: Sequelize.DATE,
        defaultValue: Sequelize.literal('NOW()')
      }
    })
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.dropTable('tuser')
  }
}

